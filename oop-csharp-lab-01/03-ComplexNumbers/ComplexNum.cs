﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return (Math.Sqrt(this.Re * this.Re + this.Im * this.Im));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, -(this.Im));
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return $"{this.GetType().Name}(Re={this.Re}, Im={this.Im})";
        }

        public static ComplexNum operator+(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re + b.Re, a.Im + b.Im);
        }

        public static ComplexNum operator -(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re - b.Re, a.Im - b.Im);
        }

        public static ComplexNum operator *(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum(a.Re*b.Re - a.Im*b.Im, a.Re*b.Im + a.Im*b.Re);
        }

        public static ComplexNum operator /(ComplexNum a, ComplexNum b)
        {
            return new ComplexNum((a.Re*b.Re + a.Im*b.Im) / (b.Re*b.Re + b.Im*b.Im), (a.Im*b.Re - a.Re*b.Im) / (b.Re * b.Re + b.Im * b.Im));
        }
    }


    static class MyExtension
    {
        public static ComplexNum Invert (ComplexNum cn)
        {
            return new ComplexNum(cn.Re, cn.Im);
        }
    }
}
